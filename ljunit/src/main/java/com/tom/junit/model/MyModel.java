package com.tom.junit.model;

/**
 * @author yuegang
 */
public class MyModel {
    private String name;

    private int age;

    public MyModel() {
    }

    public MyModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
