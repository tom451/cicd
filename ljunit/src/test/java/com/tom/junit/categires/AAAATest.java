package com.tom.junit.categires;

import com.tom.junit.listener.MyResultListener;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.junit.runner.JUnitCore;

import static org.junit.Assert.assertTrue;

/**
 * @author yuegang
 */
public class AAAATest {
    @Rule
    public TestName tagName = new TestName();

    @Test
    public void a() {
        assertTrue(true);
    }

    @Category(SlowTests.class)
    @Test
    public void b() {
        System.out.println(tagName.getMethodName());
    }

    public static void main(String[] args) {
        JUnitCore runner = new JUnitCore();
        runner.addListener(new MyResultListener());
        runner.run(AAAATest.class);
    }
}
