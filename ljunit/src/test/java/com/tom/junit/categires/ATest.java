package com.tom.junit.categires;

import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;

import static org.junit.Assert.fail;

/**
 * @author yuegang
 */
public class ATest {
    @Rule
    public TestName testName = new TestName();
    @Test
    public void a() {
//        fail();
        System.out.println(testName.getMethodName());
    }

    @Category(SlowTests.class)
    @Test
    public void b() {
    }
}
