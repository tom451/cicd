package com.tom.junit.listener;

import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

/**
 * @author yuegang
 */
public class MyResultListener extends RunListener {
    @Override
    public void testRunStarted(Description description) throws Exception {
        String className = description.getClassName();
        String methodName = description.getMethodName();
        System.out.println("------------------" + className + "." + methodName);

    }
}
