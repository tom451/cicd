package com.tom.junit.rule;

import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.*;
import org.junit.runners.MethodSorters;
import org.junit.runners.model.Statement;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LRuleTest {
    @Rule
    public TestName testName = new TestName();
    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    @ClassRule
    public static Timeout globalTimeout = Timeout.seconds(5);

    @Rule
    public TestRule testRule = (statement, description) -> new Statement() {
        @Override
        public void evaluate() throws Throwable {
            String displayName = description.getDisplayName();
            statement.evaluate();
            String className = description.getClassName();
        }
    };

    @Test
    public void test() throws IOException, InterruptedException {
        System.out.println(new Date());
        Thread.sleep(1000);
        File testFile = tmpFolder.newFile("test-file.txt");

        assertTrue("The file should have been created: ", testFile.isFile());
        assertEquals("Temp folder and test file should match: ",
                tmpFolder.getRoot(), testFile.getParentFile());
        String absolutePath = testFile.getAbsolutePath();
        System.out.println(absolutePath);
        System.out.println("tet");
    }

    @Test
    public void timeout() throws InterruptedException {
        System.out.println(new Date());
        System.out.println(testName.getMethodName());
        Thread.sleep(1000 * 1);
    }
}
