package com.tom.junit.theries;

import com.tom.junit.model.MyModel;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;

/**
 * @author yuegang
 */
@RunWith(Theories.class)
public class TheoryTest {
    @DataPoint
    public static String GOOD_USERNAME = "optimus";
    @DataPoint
    public static String USERNAME_WITH_SLASH = "optimus/prime";

    @DataPoints
    public static int[] data() {
        return new int[]{1, 5, 10, 50, 4};
    }

    @Theory
    public void filenameIncludesUsername(String username) {
        assumeThat(username, not(containsString("/")));
        assertThat(new MyModel(username).getName(), containsString(username));
    }

    @Theory
    public void testTheory(int value) {
//        Mokito
        MyModel ts = new MyModel();
        ts.setAge(value);
        assertTrue(ts.getAge() > 0);
    }
}